Gaussian blur
=============

This simple program performs a gaussian blur on given image, using «sliding window» technique, which allows to
compute resulting image with O(n × M × N ) + O(m × M × N ) complexity, where M × N stands for input image dimensions and
m, n stands for 6 * sigma (sigma is passed as a parameter to program).

Here is the example of program usage result.

Original image:

![Original image](http://storage2.static.itmages.ru/i/16/0120/h_1453318027_1467467_09b7525834.jpeg "Original")

GIMP Gaussian blur (radius = 15):

![GIMP](http://storage2.static.itmages.ru/i/16/0120/h_1453318027_7361490_d13edf5f18.jpg "GIMP")

Gblur (sigma = 5):

![gblur](http://storage2.static.itmages.ru/i/16/0120/h_1453318027_8170341_5a21d773ff.jpg "Gblur")

Downloading
-----------

You can clone this repo with git:

    git clone https://bitbucket.org/ivnglkv/gblur

## Dependencies

This program uses [libjpeg](http://ijg.org/) library.

## Compiling

In order to build this program you should have `cmake` and `make` installed. Also install development headers for libjpeg
library.

After that, build it with

    make

Output binary will be placed into `build` directory.

## Usage

    gblur -i input_file [-o output_file] [-s radius] [-h]

    Options:

    -i: path to file with input RGB JPEG image.
    -o: path to file where blurred RGB JPEG image will be recorded. If no path will be specified, input file will be overwritten!
    -s: radius of blur. Default is 5.
    -h: show this help.

Example of usage:

    gblur -i /home/user/Images/img.jpg -o /home/user/Images/blurred_img.jpg -s 5

## License

Copyright (c) 2016, Golikov Ivan

Licensed under [The BSD-3 Clause License](https://bitbucket.org/ivnglkv/gblur/raw/0521da63df805fabceff8ca0ac0a372ec24a5579/LICENSE)