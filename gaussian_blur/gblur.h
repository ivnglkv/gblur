#pragma once

#include <stdint.h>
#include <stdlib.h>
#include <jpeglib.h>

#define MAX_IMAGE_SIDE_SIZE 64000

struct image_data {
    JDIMENSION x;
    JDIMENSION y;
    uint8_t num_channels;
    unsigned char * pixels;
};

struct image_data * decompress_image(FILE *);

int8_t compress_image(struct image_data *, FILE *, uint8_t);

void destroy_image_data(struct image_data *);

int8_t gaussian_blur(struct image_data *, struct image_data *, double);
