#include <stdio.h>
#include <math.h>

#include "gblur.h"

// This function reads image pixels and some metadata from given file into struct image_data
// and returns a pointer to created struct.
// Don't forget to call destroy_image_data() after using this function!!!
struct image_data * decompress_image(FILE * file) {
    struct image_data * result;

    result = malloc(sizeof(struct image_data));

    if (result == NULL)
        return NULL;

    struct jpeg_decompress_struct image_info;
    struct jpeg_error_mgr jpeg_error;

    jpeg_create_decompress(&image_info);
    image_info.err = jpeg_std_error(&jpeg_error);

    jpeg_stdio_src(&image_info, file);

    jpeg_read_header(&image_info, TRUE);

    jpeg_start_decompress(&image_info);

    result->x = image_info.output_width;
    result->y = image_info.output_height;
    result->num_channels = image_info.num_components;


    if (result->x > MAX_IMAGE_SIDE_SIZE
        || result->y > MAX_IMAGE_SIDE_SIZE
        || result->num_channels != 3) {
        return NULL;
    }

    size_t data_size = result->x
                       * result->y
                       * result->num_channels;

    JSAMPROW buffer[1];

    result->pixels = (JSAMPROW)malloc(data_size);

    if (result->pixels == NULL)
        return NULL;

    while (image_info.output_scanline < result->y) {
//        dirty pointer math to find place where to write pixels from next line in 1D-array
        buffer[0] = result->pixels
                    + result->num_channels
                      * result->x
                      * image_info.output_scanline;

        jpeg_read_scanlines(&image_info, buffer, 1);
    }

    jpeg_finish_decompress(&image_info);
    jpeg_destroy_decompress(&image_info);

    return result;
}

int8_t compress_image(struct image_data * image, FILE * file, uint8_t quality) {
    if (image == NULL || image->pixels == NULL)
        return 1;

    struct jpeg_compress_struct image_info;
    struct jpeg_error_mgr jpeg_error;

    image_info.err = jpeg_std_error(& jpeg_error);
    jpeg_create_compress(& image_info);

    jpeg_stdio_dest(& image_info, file);

    image_info.image_width = image->x;
    image_info.image_height = image->y;
    image_info.input_components = image->num_channels;
    if (image->num_channels == 3) {
        image_info.in_color_space = JCS_RGB;
    }

    jpeg_set_defaults(& image_info);
    jpeg_set_quality(& image_info, quality, TRUE);
    jpeg_start_compress(& image_info, TRUE);

    JSAMPROW row_pointer[1];
    int row_stride;
    row_stride = image_info.image_width * image->num_channels;

    while (image_info.next_scanline < image_info.image_height) {
        row_pointer[0] = image->pixels + image_info.next_scanline * row_stride;
        jpeg_write_scanlines(& image_info, row_pointer, 1);
    }

    jpeg_finish_compress(& image_info);
    jpeg_destroy_compress(& image_info);

    return 0;
}

void destroy_image_data(struct image_data * image) {
    if (image != NULL) {
        free(image->pixels);
        free(image);
    }
}

int8_t gaussian_blur(struct image_data * original_image, struct image_data * blurred_image, double sigma) {
    if (original_image->x != blurred_image->x
        || original_image->y != blurred_image->y
        || original_image->num_channels != blurred_image->num_channels) {
        return 1;
    }

    size_t n = (size_t)ceil(3 * sigma);
    size_t window_size = n * 2 + 1;
    double window[window_size];

    window[n] = 1;
    for (size_t i = n + 1; i < window_size; ++i) {
        double foo = -pow(i - n, 2);
        double s2 = 2 * sigma * sigma;
        window[i] = exp(foo / s2);
        window[window_size - i - 1] = window[i];
    }

//    Horizontal box blur
    JSAMPROW tmp;

    JDIMENSION x = original_image->x;
    JDIMENSION y = original_image->y;
    size_t num_channels = original_image->num_channels;

    tmp = (JSAMPROW)malloc(x * num_channels);
    if (tmp == NULL) {
        return 1;
    }

    for (int j = 0; j < y; ++j) {
        for (int i = 0; i < x; ++i) {
            double sum = 0;

            double blurred_red = 0;
            double blurred_green = 0;
            double blurred_blue = 0;
            for (int k = 0; k < window_size; ++k) {
                uint64_t l = i + (k - n);
//                start index of pixel, making contribution to blurring current pixel
//                image->pixels[pixel_index], image->pixels[pixel + 1], image->pixels[pixel + 2] are holding
//                values of red, green and blue colors of pixel respectively
                uint64_t pixel_index = (num_channels * x) * j + num_channels * l;

                if (l >= 0 && l < x) {
                    blurred_red += original_image->pixels[pixel_index] * window[k];
                    blurred_green += original_image->pixels[pixel_index + 1] * window[k];
                    blurred_blue += original_image->pixels[pixel_index + 2] * window[k];

                    sum += window[k];
                }
            }

            blurred_red = floor(blurred_red / sum);
            blurred_green = floor(blurred_green / sum);
            blurred_blue = floor(blurred_blue / sum);

            tmp[num_channels * i] = (unsigned char)blurred_red;
            tmp[num_channels * i + 1] = (unsigned char)blurred_green;
            tmp[num_channels * i + 2] = (unsigned char)blurred_blue;
        }

        for (int m = 0; m < num_channels * x; ++m) {
            blurred_image->pixels[m+ j * x * num_channels] = tmp[m];
        }
    }

    JSAMPROW t = realloc(tmp, y * num_channels);
    if (t == NULL) {
        free(tmp);
    } else {
        tmp = t;
    }

//    Vertical box blur
    for (int i = 0; i < x; ++i) {
        for (int j = 0; j < y; ++j) {
            double sum = 0;

            double blurred_red = 0;
            double blurred_green = 0;
            double blurred_blue = 0;
            for (int k = 0; k < window_size; ++k) {
                uint64_t l = j + (k - n);
                uint64_t pixel = num_channels * x * l + num_channels * i;

                if (l >= 0 && l < y) {

                    blurred_red += blurred_image->pixels[pixel] * window[k];
                    blurred_green += blurred_image->pixels[pixel + 1] * window[k];
                    blurred_blue += blurred_image->pixels[pixel + 2] * window[k];

                    sum += window[k];
                }
            }

            blurred_red = floor(blurred_red / sum);
            blurred_green = floor(blurred_green / sum);
            blurred_blue = floor(blurred_blue / sum);

            tmp[num_channels * j] = (JSAMPLE)blurred_red;
            tmp[num_channels * j + 1] = (JSAMPLE)blurred_green;
            tmp[num_channels * j + 2] = (JSAMPLE)blurred_blue;
        }

        for (int m = 0; m < num_channels * y; m+=3) {
            int column = m / 3;
            blurred_image->pixels[num_channels * x * column + num_channels * i] = tmp[m];
            blurred_image->pixels[num_channels * x * column + num_channels * i + 1] = tmp[m + 1];
            blurred_image->pixels[num_channels * x * column + num_channels * i + 2] = tmp[m + 2];
        }
    }
    free(tmp);

    return 0;
}
