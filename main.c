#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <getopt.h>
#include <unistd.h>
#include "gblur.h"

void print_help() {
    printf("This program performs gaussian blur on given image using sigma parameter.\n"
                   "\n"
                   "Usage:\n"
                   "\tgblur -i input_file [-o output_file] [-s radius] [-h]\n"
                   "Options:\n"
                   "\t-i: path to file with input RGB JPEG image.\n"
                   "\t-o: path to file where blurred RGB JPEG image will be recorded. If no path will be specified, "
                   "input file will be overwritten!\n"
                   "\t-s: radius of blur. Default is 5.\n"
                   "\t-h: show this help.\n"
                   "\n"
                   "Example of usage:"
                   "\tgblur -i /home/user/Images/img.jpg -o /home/user/Images/blurred_img.jpg -s 5\n");
}

int main(int argc, char *argv[]) {
    int opt;
    double sigma = 0.0;
    char *input_filename = NULL;
    char *out_filename = NULL;

//    Parsing passed arguments
    while ((opt = getopt(argc, argv, "i:o:s:h")) != -1) {
        switch(opt) {
            case 'i':
                input_filename = optarg;
                break;
            case 'o':
                out_filename = optarg;
                break;
            case 's':
                sigma = atof(optarg);
                break;
            case 'h':
                print_help();
                exit(0);
                break;
            case ':':
                printf("Option needs a value\n");
                break;
            case '?':
                printf("Unknown option\n");
                break;
            default:
                break;
        }
    }

    if (input_filename) {
        if (access(input_filename, R_OK) == -1) {
            fprintf(stderr, "Can not read file %s\n", input_filename);
            exit(1);
        }
    } else {
        fprintf(stderr, "Please specify input filename!\n");
        print_help();
        exit(1);
    }

    if (sigma == 0.0) {
        printf("Value of sigma is 0, so using default value: 5.0\n");
        sigma = 5.0;
    }

    FILE *infile;

    if ((infile = fopen(input_filename, "rb")) == NULL) {
        fprintf(stderr, "Can not open %s\n", input_filename);
        exit(1);
    }

    struct image_data * original_image = decompress_image(infile);
    fclose(infile);

    if (original_image == NULL) {
        fprintf(stderr, "Failed to decompress image %s\n", input_filename);
        exit(1);
    }

//    If no output file were specified, use original image
    FILE *outfile;
    if (out_filename == NULL) {
        out_filename = input_filename;
    }

    if ((outfile = fopen(out_filename, "wb")) == NULL) {
        fprintf(stderr, "Can not open %s\n", out_filename);
        exit(1);
    }

    struct image_data * blurred_image;
    blurred_image = malloc(sizeof(struct image_data));

    if (blurred_image == NULL) {
        fprintf(stderr, "Failed to allocate memory!\n");

        destroy_image_data(original_image);

        exit(1);
    }

    blurred_image->x = original_image->x;
    blurred_image->y = original_image->y;
    blurred_image->num_channels = original_image->num_channels;

    size_t data_size = blurred_image->x
                       * blurred_image->y
                       * blurred_image->num_channels;

    blurred_image->pixels = (unsigned char *)malloc(data_size);

    int8_t blur_result;

    blur_result = gaussian_blur(original_image, blurred_image, sigma);

    if (blur_result != 0) {
        fprintf(stderr, "Failed to blur image!\n");

        destroy_image_data(original_image);
        destroy_image_data(blurred_image);

        exit(1);
    }

    int8_t compress_result;
    compress_result = compress_image(blurred_image, outfile, 90);

    fclose(outfile);

    if (compress_result != 0) {
        fprintf(stderr, "Failed to write blurred image to file!\n");

        destroy_image_data(original_image);
        destroy_image_data(blurred_image);

        exit(1);
    }

    destroy_image_data(original_image);
    destroy_image_data(blurred_image);

    return 0;
}
